package Ferzle;

/**
 * @author Charles Cusack
 * @version 1.0, September 2006
 * 
 * The controller class for use with the FerzleModel class, and
 * the associated view.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FerzleController implements ActionListener {

	// The controller needs to know about the model so it can update it.
    private FerzleModel myModel;

	// The controller needs to know about the view so it can react to
	// user input.
    private FerzleView myView;
    
    /**
     * The constructor
     * @param model the model which the controller will control
     */
    public FerzleController(FerzleModel model,FerzleView view) {
        myModel=model;
        myView=view;
        view.addController(this); // So this can react to user input.
    }
    
    /** 
     * From the ActionListener interface.
     * Update the model with the text from the input field.
     *
     * @param e the event
     */
    public void actionPerformed(ActionEvent e) {
        String theFerzle= myView.getInput();
        if(theFerzle!=null) {
            myModel.setFerzle(theFerzle);
        }
    }
}
