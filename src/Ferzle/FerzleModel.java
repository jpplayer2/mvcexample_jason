package Ferzle;

/**
 * @author Charles Cusack
 * @version 1.0, September 2006
 * Modified, February, 2008
 *
 * A very simple model.  It just holds a String.
 */
import javax.swing.event.EventListenerList;

public class FerzleModel {
    
    // The data for this model
    private String ferzle;
    
    // Create the listener list.  This is a list of things that want
    // to be informed if any changes occur in this model.
    private EventListenerList listenerList = new EventListenerList();
    
    /**
     * The constructor, assuming an empty model
     */
    public FerzleModel() {
	    ferzle=new String("");
    }   
    
    /**
     * The accessor method for the Ferzle object
     * @return The Ferzle object
     */
    public String getFerzle() {
    	return ferzle;
    }
    
    /**
     * Mutator method.
     * @param ferzle the data element Ferzle in the model.
     */
    public void setFerzle(String ferzle) {
       this.ferzle=ferzle;	
       // Any time the value of ferzle changes, we have to
       // fire an event to inform anyone who is listening
       fireFerzleEvent(new FerzleEvent(this));
    }
   // This methods allows classes to register for FerzleEvents
    public void addFerzleListener(FerzleListener listener) {
        listenerList.add(FerzleListener.class, listener);
    }

    // This methods allows classes to unregister for FerzleEvents
    public void removeFerzleListener(FerzleListener listener) {
        listenerList.remove(FerzleListener.class, listener);
    }

    // This private class is used to fire FerzleEvents
    void fireFerzleEvent(FerzleEvent evt) {
        Object[] listeners = listenerList.getListenerList();
        // Each listener occupies two elements - the first is the listener class
        // and the second is the listener instance
        for (int i=0; i<listeners.length; i+=2) {
            if (listeners[i]==FerzleListener.class) {
                ((FerzleListener)listeners[i+1]).ferzleDataChanged(evt);
            }
        }
    }
}
