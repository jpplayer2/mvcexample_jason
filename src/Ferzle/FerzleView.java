package Ferzle;

/**
 * @author Charles Cusack
 * @version 1.0, September 2006
 * 
 * A view to go along with an FerzleModel and FerzleController
 * In this implementation, the view has all of the graphical 
 * components on it, and the controller is just a "background" class.
 * Another way to think about this is to have the controller be
 * graphical and process its events.  
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class FerzleView extends JPanel implements FerzleListener {
	   
	// The input box and button.
    private JTextField input;
    private JButton theButton;
    
    // The data will be displayed on this
    private JTextField output;   

    // The view needs to know about the model so when it is informed
    // of changes, it can get the updated information from the model.
    FerzleModel myModel;
    
    /**
     * The constructor.  It sets up the graphical objects.
     * 
     * @param model The model which this view will base its output on.
     */
    public FerzleView(FerzleModel model) {
        super();

        // Add the view as a listener to the model.
	    // Now it can react to the model as needed.
        myModel=model;
        myModel.addFerzleListener(this);
        
        // Instantiate and set up the input field and button
        input=new JTextField(10);
        input.setBorder(new TitledBorder("Input"));
        theButton=new JButton("Update Ferzle");
        
        // put the input field and button on a panel with nice title.
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(input);       
        panel.add(theButton);   
        panel.setBorder(new TitledBorder("I am the input.  The Controller listens to me."));  
        
        output=new JTextField(10);
        output.setEditable(false);
        output.setBorder(new TitledBorder("I am the output.  I listen to the Model."));
        
        // Now put the input and output on the main panel.
        setLayout(new BorderLayout());
        add(panel,BorderLayout.NORTH);
        add(output,BorderLayout.SOUTH);
   
    }
    /**
     * Accessor method to get input field data
     * @return the contents of the input field
     */
    public String getInput() {
    	return input.getText();
    }
    
    /**
     * Adds an actionListener to the button.
     * 
     * @param controller the object which want to recieve button clicks.
     */
    public void addController(ActionListener controller) {
    	 theButton.addActionListener(controller);
    }
    
    /** 
     * From FerzleListener interface
     * Simple re-draw the data.
     * @param e the event
     */
    public void ferzleDataChanged(FerzleEvent e) {
	    output.setText(myModel.getFerzle());
    }
}
