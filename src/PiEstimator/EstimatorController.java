package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * The controller class for use with an EstimatorModel class, and
 * any associated view(s).
 * It allows the user to choose the number of data points, the first
 * data point, and the interval between data points.
 * <p>
 * The controller has a single model associated with it, and informs the
 * model when the input changes.
 *
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
/** 
 * 
 */
public class EstimatorController extends JPanel implements ActionListener {
    
    // The graphical components of the controller
    private JTextField numberOfEstimates;
    private JTextField firstEstimate;
    private JTextField estimateInterval;
    private JButton theButton;
    
    // The model associated with this controller.  That is, the model which
    // this controller will inform of any input.
    private EstimatorModel myModel;
    
    //-----------------------------------------------------------------------
    /**
     * The constructor
     * @param model the model which the controller will control
     */
    public EstimatorController(EstimatorModel model) {
        
        myModel=model;  // Assign the model
        
        // Instantiate and set up the input fields
        numberOfEstimates=new JTextField(6);
        numberOfEstimates.setBorder(new TitledBorder("points"));
        firstEstimate=new JTextField(6);
        firstEstimate.setBorder(new TitledBorder("first"));
        estimateInterval=new JTextField(6);
        estimateInterval.setBorder(new TitledBorder("intervals"));
        
        // Instantiate and setup the button
        theButton=new JButton("Press me");
        theButton.addActionListener(this);
        
        // Place the graphical components on a box, which is placed 
        // on the panel, and create a nice border.
        Box theBox=Box.createHorizontalBox();
        theBox.add(numberOfEstimates);   
        theBox.add(firstEstimate);        
        theBox.add(estimateInterval);
        theBox.add(theButton);
        add(theBox);
        this.setBorder(new BevelBorder(BevelBorder.RAISED,
                                Color.gray,Color.lightGray));
    }
    //-----------------------------------------------------------------------
    /** 
     * From the ActionListener interface.
     * Get each of the numbers from the input fields, and tell
     * the model they have been changed.
     * @param e the event
     */
    public void actionPerformed(ActionEvent e) {
        try {
            int number=Integer.parseInt(numberOfEstimates.getText());
            int first=Integer.parseInt(firstEstimate.getText());          
            int interval=Integer.parseInt(estimateInterval.getText());
            myModel.setParameters(number,first,interval);
        }
        catch (NumberFormatException f) {
        }
    }
}
