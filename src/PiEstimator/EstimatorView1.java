package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * A view to go along with an EstimatorModel and EstimatorController
 * It displays the data as a list, showing the indices and estimates.
 */
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class EstimatorView1 extends JPanel implements EstimatorListener {
    
    // The Model which the object listens to.
    private EstimatorModel myModel;
    
    // The data will be displayed on this
    private JTextArea output;  
    
    //--------------------------------------------------------------------
    /**
     * The constructor.  It sets up the graphical objects.
     */
    public EstimatorView1() {
        super();
        output=new JTextArea(25,20);
        output.setEditable(false);
        this.add(new JScrollPane(output));
    }
    
    /**
     * Get the data from the model and draw it on the panel.
     */
    public void drawData() {
        String theData="";
        int first=myModel.getFirstEstimate();
        int number=myModel.getNumberOfEstimates();
        int interval=myModel.getEstimatorInterval();
        for(int i=0;i<number;i++) {
            theData += (first + i*interval ) + "  " + 
                        myModel.getEstimate(i) + "\n";
        }
        output.setText(theData);
        this.validate();
    }
    
    /** 
     * From EstimatorListener interface
     * Simple re-draw the data.
     * @param e the event
     */
    public void estimatorDataChanged(EstimatorEvent e) {
        if(e.getSource() instanceof EstimatorModel) {            
            myModel= (EstimatorModel) e.getSource();
            drawData();
        }
    }
}