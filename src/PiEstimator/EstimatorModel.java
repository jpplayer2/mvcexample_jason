package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * The EstimatorModel is the model of an estimator.  It is an abstract class,
 * leaving the details of the estimator to the subclasses.
 * The model assumes there is a formula f(i) to compute the ith estimate
 * of the number to be estimated.  The model stores the first index which
 * is desired, the number of estimates to store, and the interval between 
 * intervals. For instance, if
 *      numberOfEstimates=4
 *      firstEstimate=3
 *      estimateInterval=5
 * then the model will store f(3), f(8), f(13), and f(18).
 *
 * The model maintains a list of object which are registered as listeners
 * to this class.  If the model changes, this class will inform each of
 * the registered listeners of the fact that the model has changed, leavinf
 * it up to each of them what they want to do about it.
 *
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class EstimatorModel {
    
    /*
     * The only abstract method that must be overriden.
     * It should re-compute the values of the estimates array.
     * It should not assume any of the current data is valid.
     * In particular, the array size may need to be changed.
     */
    abstract public void recalculateEstimates();
    
    // The array of data estimates
    protected double estimates[];
    
    // The 3 paramaters determining which estimates will be stored
    private int numberOfEstimates;
    private int firstEstimate;
    private int estimateInterval;
    
    // The list of registered listeners
    protected Set estimatorListeners;
    
    /**
     * The constructor, assuming an empty model
     */
    public EstimatorModel() {
        estimates=null;
        numberOfEstimates=0;
        firstEstimate=0;
        estimateInterval=0;
        estimatorListeners=new HashSet(1);
    }   

    /**
     * Get the nth estimate from the list
     * @param n the index of the desired estimate
     *   Note: This is asking for the nth estimate that is stored in the
     *   model, not f(n), where f is the function that computes the estimates.
     * @returns the nth estimate on the list. 
     *    If n is not between 0 and numberOfEstimates, it returns 0.
     *    It should really throw and exception, but for simplicity, and
     *    since the point of the example is to demostrate MVC, not exception
     *    handling, simple is better.
     */
    public double getEstimate(int n) {
        if(n>=0 && n<numberOfEstimates) {
            return estimates[n];
        }
        else {
            return 0;
        }
    }
    
    /**
     * @return the maximum of the stored estimates
     */
    public double getMaximumEstimate() {
        double max=estimates[0];
        for(int i=1;i<numberOfEstimates;i++) {
            if(estimates[i]>max) {
                max=estimates[i];
            }
        }
        return max;
    }
    
    /*
     * @return the minimum of the stored estimates
     */
    public double getMinimumEstimate() {
        double min=estimates[0];
        for(int i=1;i<numberOfEstimates;i++) {
            if(estimates[i]<min) {
                min=estimates[i];
            }
        }
        return min;
    }
    /**
     * @returns the number of stored estimates
     */
    public int getNumberOfEstimates() {
        return numberOfEstimates;
    }
    /**
     * Sets the number of estimates to n
     * @param n the number of desired estimates
     */
    public void setNumberOfEstimates(int n) {
        numberOfEstimates=n;
        dataChanged();
    }
    /**
     * Get the index of the first stored estimate
     * @return the index of the first estimate
     */
    public int getFirstEstimate() {
        return firstEstimate;
    }
    /**
     * Set the index of the first estimate to n
     * @param n the desired index of the first estimate
     */
    public void setFirstEstimate(int n) {
        firstEstimate=n;
        dataChanged();
    }
    /**
     * @return the interval between estimates
     */
    public int getEstimatorInterval() {
        return estimateInterval;
    }
    /**
     * Set the desired interval between estimates to n
     * @param n the desried interval between estimates.
     */
    public void setEstimateInterval(int n) {
        estimateInterval=n;
        dataChanged();
    }
    /**
     * Set all of the parameters of the model
     * @param n desired number of estimates
     * @param f desired index of first index
     * @param i desired interval between estimates
     */
    public void setParameters(int n,int f,int i) {
        numberOfEstimates=n;
        firstEstimate=f;
        estimateInterval=i;
        dataChanged();
    }
    //---------------------------------------------------------------------
    // The following methods relate to the EstimatorListeners
    //
    /**
     * Add a listener the the list
     * @param listener the object desiring to listen to this class
     */
    public void addEstimatorListener(EstimatorListener listener) {
        estimatorListeners.add(listener);   
    }
    /**
     * If any of the data in the model changes, this method takes care
     * of recalculating the estimates, and informing the listeners of
     * the fact that the model has changed.
     */
    public void dataChanged() {
        recalculateEstimates();
        Iterator iterator=new HashSet(estimatorListeners).iterator();
        while(iterator.hasNext() ) {
            EstimatorListener listener=(EstimatorListener) iterator.next();
            listener.estimatorDataChanged(new EstimatorEvent(this));
        }
    }
}
