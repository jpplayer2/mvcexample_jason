package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * A view to be used with EstimatorModel and EstimatorController
 * It displays the data on a graph.
 */
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class EstimatorView2 extends JPanel implements EstimatorListener {
   
    // The width and height of the panel
    private static final int WIDTH=400;
    private static final int HEIGHT=400;
    
    // Various data that helps to scale the data
    private double minY;
    private double maxY;
    private double vScale;
    private double hScale;
    
    // The model which this view is representing
    private EstimatorModel myModel;

    /**
     * The plain-jane constructor
     */
    public EstimatorView2() {
        super();
        myModel=null;
        repaint();
    }
    
    /**
     * Overriden paintComponent, used to display the graph on the panel.
     * @param g the graphics object used on the panel.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(myModel!=null) {
            // Obtain the parameters from the model
            int first=myModel.getFirstEstimate();
            int number=myModel.getNumberOfEstimates();
            int interval=myModel.getEstimatorInterval();
                      
            // Draw the graph
            for(int i=0;i<number-1;i++) {
                // Get some data to help scale
                minY=myModel.getMaximumEstimate();
                maxY=myModel.getMinimumEstimate();
                vScale=HEIGHT*1.0/(maxY-minY);
                
                // Scale the data to fit the graph
                int x1=(int) (20+WIDTH*i/(number-1));
                int y1=(int) (15+(myModel.getEstimate(i)-minY)*vScale);
                int x2=(int) (20+WIDTH*(i+1)/(number-1));
                int y2=(int) (15+(myModel.getEstimate(i+1)-minY)*vScale);

                // Draw the data line
                g.setColor(Color.red);
                g.drawLine(x1,y1,x2,y2);
                
                // Draw the hash marks at the bottom
                g.setColor(Color.black);
                g.drawLine(x1,HEIGHT+30,x1,HEIGHT+40);
                g.drawLine(x2,HEIGHT+30,x2,HEIGHT+40);
                }
            
            // Draw the bounding box, and some data points
            g.drawRect(20,15,WIDTH,HEIGHT+25);
            g.setColor(Color.blue);
            g.drawString(""+myModel.getMaximumEstimate(),5,30); 
            g.drawString(""+myModel.getMinimumEstimate(),5,HEIGHT+5);                                           
            int y=(int) (15+(myModel.getEstimate(number-1)-minY)*vScale);
            g.drawString(""+myModel.getEstimate(number-1),5,y);
        }   
    }

    /**
     * From EstimatorListener interface
     * Re-draw the data
     * @param e the event
     */
    public void estimatorDataChanged(EstimatorEvent e) {
        if(e.getSource() instanceof EstimatorModel) {            
            myModel= (EstimatorModel) e.getSource();         
            repaint();
        }
    }
}
