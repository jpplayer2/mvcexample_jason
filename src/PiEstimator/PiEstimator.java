package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * The PiEstimator, which brings the model, view, and controller together
 * to form the final applet.
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

public class PiEstimator extends JFrame {
    
    // The model, controller, and 2 different views
    private PiEstimatorModel model;
    private EstimatorController controller;
    private EstimatorView1 view1;
    private EstimatorView2 view2;

    public static void main(String[] args) {  
    	new PiEstimator();
     } 
    
    public PiEstimator() {
        //Instantiate the model
        model=new PiEstimatorModel();
        
        // instantiate the controller
        controller=new EstimatorController(model);     
        
        // Instantiate the first view, add border
        view1=new EstimatorView1();
        view1.setBorder(new TitledBorder("List View"));
        
        // Instantiate the second view, add border
        view2=new EstimatorView2();
        view2.setBorder(new TitledBorder("Graph View"));
        
        // Add each of the views as listeners to the model.
        model.addEstimatorListener(view1);
        model.addEstimatorListener(view2);

        setTitle("A useless application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
        Container container=getContentPane();        
        container.setLayout(new BorderLayout());
        container.add(view1,BorderLayout.WEST);
        container.add(view2,BorderLayout.CENTER);        
        container.add(controller,BorderLayout.SOUTH);
        
        // Add a title/description to the applet
        JTextArea description=new JTextArea();
        description.setAlignmentX(JTextArea.CENTER_ALIGNMENT);
        description.setFont(new Font("Times Roman",Font.BOLD,18));
        description.setText("Estimates pi according to the formula:  "+
               "PI=4-(4/3)+(4/5)-(4/7)+(4/9)-(4/11)+...");
        container.add(description,BorderLayout.NORTH);

        // Finish setting up the main window
        setBackground(Color.white);
        pack(); 
        setSize(new Dimension(800,600));
        setVisible(true);   
    }
    
}
