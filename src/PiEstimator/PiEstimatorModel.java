package PiEstimator;

/**
 * @author Charles Cusack
 * @version 1.0, Octber 5, 2002
 * 
 * A subclass of EstimatorModel that estimates the value of PI by using
 * the formula pi=sum_{i=0}^{n} (-1)^i * 4 / (2*i-1).
 *
 */
public class PiEstimatorModel extends EstimatorModel {
    
    /**
     * The plain-jane constructor.
     */
    public PiEstimatorModel() {
        super();
    }
    /**
     * Computes the partial sum to estimate pi.
     * @param n the last index of the sum
     */
    public double computePiEstimate(int n) {
        double piEst=0.0;
        for(int i=0;i<=n;i++) {
            piEst += Math.pow(-1,i)*4.0/(2.0*i+1.0);
        }
        return piEst;
    }
        
    /** 
     * From the abstract classes
     * recalculates each of the estimates, based on the
     * formula, and the parameters.
     */
    public void recalculateEstimates() {
        estimates=new double[getNumberOfEstimates()];
        for(int i=0;i<getNumberOfEstimates();i++) {
            estimates[i]=computePiEstimate(getFirstEstimate()+
                          i*getEstimatorInterval());
        }

    }
}
